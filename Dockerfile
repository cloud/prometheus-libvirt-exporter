FROM registry.gitlab.ics.muni.cz:443/cloud/container-registry/centos:7

ARG VERSION=unknown-version
ARG BUILD_DATE=unknown-date
ARG CI_COMMIT_SHA=unknown
ARG CI_BUILD_HOSTNAME
ARG CI_BUILD_JOB_NAME
ARG CI_BUILD_ID

COPY prometheus-libvirt-exporter/dist/prometheus-libvirt-exporter_linux_amd64/prometheus-libvirt-exporter /usr/local/bin

RUN yum -y update && \
    yum clean all

ENTRYPOINT ["/usr/local/bin/prometheus-libvirt-exporter"]

LABEL maintainer="MetaCentrum Cloud Team <cloud[at]ics.muni.cz>" \
      org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Masaryk University, ICS" \
      org.label-schema.name="prometheus-libvirt-exporter" \
      org.label-schema.version="$VERSION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-ci-job-name="$CI_BUILD_JOB_NAME" \
      org.label-schema.build-ci-build-id="$CI_BUILD_ID" \
      org.label-schema.build-ci-host-name="$CI_BUILD_HOSTNAME" \
      org.label-schema.url="https://gitlab.ics.muni.cz/cloud/prometheus-libvirt-exporter" \
      org.label-schema.vcs-url="https://gitlab.ics.muni.cz/cloud/prometheus-libvirt-exporter" \
      org.label-schema.vcs-ref="$CI_COMMIT_SHA"
