# prometheus-libvirt-exporter

Downstream build and release of upstream https://github.com/zhangjianweibj/prometheus-libvirt-exporter

## How to release new version
 1. [Check last upstream version](https://github.com/zhangjianweibj/prometheus-libvirt-exporter/releases).
 1. Update [CHANGELOG.md](/CHANGELOG.md) with same upstream version including one of possible suffixes:
    * release tagged version: `-<release-integer>` suffix to be able to release multiple versions based on same upstream code. (`<release-integer>` is integer > 0)
    * release particular commit version: `-<short-commit-hash>-<release-integer>` suffix to be able to release multiple versions based on same upstream code.
 1. Tag repo with composed semver v2 compatible tag. (for instance `1.2.0-e66d6b7-1` or `1.3.0-1`)
