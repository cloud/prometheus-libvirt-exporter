# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0-e66d6b7-2] - 2021-07-22
### Changed
- Release based on Centos7 (debugging, networking problems)

## [1.2.0-e66d6b7-1] - 2021-07-21
### Added
- Initial release
